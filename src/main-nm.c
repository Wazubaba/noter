#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "add-nm.h"
#include "rem-nm.h"
#include "set-nm.h"
#include "get-nm.h"
#include "cfg.h"
#include "common.h"
#include "halp.h"

char * datpath;

int main(int argc, char *argv[], char *envp[]){
	if(argc < 2){
		// todo: write help display function
		halp("");
		return 1;
	}
	else{
		if(!strcmp("help",argv[1])){
			if(argc>2){
				halp(argv[2]);
			} else {
				halp("");
			}
		}
		else if(!strcmp("add",argv[1])){
			// todo: execute the add system
			ENTRY entry;
			entry.name = "Fish";
			entry.category = "animals";
			entry.data = "hey look a chicken.";
			add(entry);
//			puts("add");
		}
		else if(!strcmp("rem",argv[1])){
			// todo: execute the rem system
			ENTRY entry;
			entry.name = "fish";
			entry.category = "dogwaffle";
			entry.data = "ohemgee";
			rem(entry);
//			puts("rem");
		}
		else if(!strcmp("set",argv[1])){
			// todo: execute the conf system
			set(argc, argv);
		}
		else if(!strcmp("get",argv[1])){
			// todo: execute the find system
			get(argv[0]);
		}
		else if(!strcmp("test",argv[1])){
//			getConfig("test.cfg");
//			int i;
//			for(i=0;i<4;i++){
//				printf("%s=%s\n",cfgdat[i].key,cfgdat[i].val);
//			}
			char * datpath = getDatPath();
			puts(datpath);
		}
		else{
			// todo: write error display function
			puts("error");
		}
	}
	free(datpath);
	return 0;
}
