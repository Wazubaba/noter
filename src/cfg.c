#include <stdio.h>
#include <string.h>
#include "cfg.h"

int getConfig(char *fn){
	FILE *cfgi;
	char buffer[256];
	char work[256]; // tokenized string
	int c=0; // current char iterator for tokenizer
	int i=0; // iterator for tokenizer
	int index=0; // position in the data table
	kvpair set;

	cfgi = fopen(fn,"r");
	if(cfgi == NULL){
		fprintf(stderr,"[cfg:error reading %s]\n",fn);
		return 1;
	}
	puts("[cfg:config opened...]");
	while(fscanf(cfgi,"%s",buffer) != EOF){
		for(i=0;i<strlen(buffer);i++){
			if(buffer[i] == '='){
				work[c] = '\0'; // we would be on the '=' char here, so set it to \0
				strcpy(cfgdat[index].key,work);

				c = 0; // reset our position in the string
				memset(work,0,sizeof(work));
			} else {
				work[c] = buffer[i];
				c++;
			}
		}
		work[c] = '\0'; // set the \n to \0
		strcpy(cfgdat[index].val, work);

		c=0; // reset our iterator
		index++;
		memset(work, 0, sizeof(work));
	}
	puts("[cfg:read complete.]");
	if(cfgi != NULL){
		fclose(cfgi);
		puts("[cfg:closing...]");
	} else {
		puts("[cfg WARN: Error closing opened config file... Is something locking it open?]");
		return 2;
	}
	return 0;
}

