#ifndef cfg
#define cfg

#include <stdio.h>
#include <string.h>

typedef struct{
	char key[256];
	char val[256];
} kvpair;

kvpair cfgdat[256];

int getConfig(char *fn);

#endif
