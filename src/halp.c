#include "halp.h"

void halp(char *mod){
	printf("Noteman    -    help\n");
	if(strcmp(mod,"add") > 0){
		halpAdd();
	} else
	if(strcmp(mod,"rem") > 0){
		halpRem();
	} else
	if(strcmp(mod,"get") > 0){
		halpGet();
	} else
	if(strcmp(mod,"set") > 0){
		halpSet();
	} else {
		halpMain();
	}
}

void halpMain(void){
	printf(
"\
Noteman consists of several\n\
submodules. Available topics:\n\
=============================\n\
\tadd\t-\tadd a note\n\
\trem\t-\tremove a note\n\
\tset\t-\tchange configuration\n\
\tget\t-\tbrowse and view notes\n\n\
You can get help for individual submodules by executing\n\
\tnm help <submodule>\n\
"
);

}

void halpAdd(void){
	printf("Noteman\t-\tadd module\n");
}

void halpRem(void){
	printf("Noteman\t-\trem module\n");
}

void halpSet(void){
	printf("Noteman\t-\tset module\n");
}

void halpGet(void){
	printf("Noteman\t-\tget module\n");
}
