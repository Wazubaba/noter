#ifndef HALP
#define HALP

#include <string.h>
#include <stdio.h>

void halpAdd(void);
void halpSet(void);
void halpGet(void);
void halpRem(void);
void halpMain(void);
void halp(char *mod);

#endif
