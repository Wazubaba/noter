#include <stdio.h>
#include <string.h>
//#include <stdlib.h>
//#include "cfg.h"

typedef struct{
	char key[256];
	char val[256];
} kvpair;

static kvpair cfgdat[256]; // hard-limiting to 256 indexes
//////////////////
int get(char * fn);

void main(int argc, char *argv[]){ // note: this will be removed for the library variant of this module
	get("./test.cfg");
	puts("cfg:GET complete... data is as follows:]");
	int i;
	for(i=0;i<3;i++){
		printf("%s=%s\n",cfgdat[i].key,cfgdat[i].val);
	}
}


int get(char *fn){
	FILE *cfgi;
	char buffer[256];
	char work[256]; // tokenized string
	int c=0; // current char iterator for tokenizer
	int i=0; // iterator for tokenizer
	int index=0; // position in the data table
	kvpair set;

	cfgi = fopen(fn,"r");
	if(cfgi == NULL){
		fprintf(stderr,"[cfg:error reading %s]\n",fn);
		return 1;
	}
	puts("[cfg:config opened...]");
	while(fscanf(cfgi,"%s",buffer) != EOF){
		for(i=0;i<strlen(buffer);i++){
			if(buffer[i] == '='){
				work[c+1] = '\0'; // complete the string
				strcpy(cfgdat[index].key,work);

				c = 0; // reset our position in the string
				memset(work,0,sizeof(work));
			}/* else 
			if(buffer[i] == '#'){ // attempt to add comment support to the configs...
				if(strlen(cfgdat[index].key) > 0){
					break;
				}
			}*/ else {
				work[c] = buffer[i];
				c++;
			}
		}
		work[c] = '\0'; // set the \n to \0
		strcpy(cfgdat[index].val, work);

		c=0; // reset our iterator
		index++;
		memset(work, 0, sizeof(work));
	}
	puts("[cfg:read complete.]");
	if(cfgi != NULL){
		fclose(cfgi);
		puts("[cfg:closing...]");
	} else {
		puts("[cfg WARN: Error closing opened config file... Is something locking it open?]");
		return 2;
	}
	return 0;
}

