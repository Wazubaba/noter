targ="global"

all:meow

meow:
	@echo "Please specify global or local compile target"

dog:
ifeq ($(targ),"global")
	@echo "Goldfish"
	targ="taco"
	@echo $(targ)
endif
	@echo $(targ)

global:
	@echo "Performing initial directory structure test..."
	@if [ ! -d ./bin ]; then echo "Creating bin directory..."; mkdir bin; fi
	@if [ ! -d ./bld ]; then echo "Creating bld directory..."; mkdir bld; fi
	@echo "===================="
	@echo "+Compiling Noter...+"
	@echo "===================="
	gcc -c -o bld/halp.o src/halp.c
	gcc -c -o bld/common.o src/common.c
	gcc -c -o bld/cfg.o src/cfg.c
	gcc -c -o bld/add-nm.o src/add-nm.c
	gcc -c -o bld/rem-nm.o src/rem-nm.c
	gcc -c -o bld/set-nm.o src/set-nm.c
	gcc -c -o bld/get-nm.o src/get-nm.c
	gcc -c -o bld/main-nm.o src/main-nm.c
	gcc -o bin/nm bld/halp.o bld/common.o bld/cfg.o bld/add-nm.o bld/rem-nm.o bld/set-nm.o bld/get-nm.o bld/main-nm.o
	@echo "Compile complete! Now run make install as root or via sudo for a"
	@echo "system-wide installation or make local for a local installation."

cfgs:
	@echo "Generating test database for nfm test... dbtest.ndb"
	@echo "test\nfish\ngoat" > bin/dbtest.ndb
	@echo "Generating test config for cfg... test.cfg"
	@echo "goat=milkfish\ntaco=salad\nmeow=mix\ndog=water\n" > bin/test.cfg

test:
	@echo "Generating tests..."
#	gcc -c -o bld/common.o src/common.c
	gcc -c -o bld/test.o tests/test.c
	gcc -c -o bld/nfm.o tests/nfm.c
	gcc -o bin/test bld/test.o
	gcc -o bin/nfm bld/nfm.o
	gcc -o bin/interface tests/interface.c -lncurses
	@echo "Generating test database for nfm test... test.ndb"
	@echo "test\nfish\ngoat" > bin/test.ndb
	gcc -o bin/cfg tests/cfg.c
	@echo "Generating test config for cfg... test.cfg"
	@echo "goat=milkfish\ntaco=salad\nmeow=mix\n" > bin/test.cfg

install:
	@echo "Installing Noter as system-wide..."
	#@mkdir /usr/bin/noter
	#@cp ./bin/* /usr/bin/noter/.
	@echo "Generating default configs..."
	#@mkdir $HOME/.config/noter
	#@echo "test\nrawr\nnya\n" > $HOME/.config/noter/settings.cfg
	@echo "======================================================"
	@echo "+ Install complete! It is suggested that you execute +"
	@echo "+ make clean to purge un-needed build files next. If +"
	@echo "+ you want to uninstall Noter, you can perform 'make +"
	@echo "+ uninstall' as root or via sudo to remove either of +"
	@echo "+ the installs.                                      +"
	@echo "======================================================"
	

local:
	@echo "Building Noter locally..."
	#todo: learn how to use cmake so we can add different compile flags
	#plan b: cmake seems weird, why not just use sed to edit a var in common.h?
	@sed "s/#define GLOBAL/#define LOCAL/" < src/common.h > src/TEMP #edit src to set local mode
	@mv src/TEMP src/common.h #looks like this is needed...
	@mkdir ./noter
	@mv bin/nm noter/.
	@touch noter/config
	@sed "s/#define LOCAL/#define GLOBAL/" < src/common.h > src/TEMP #revert the change. <3 sed the entire time
	@mv src/TEMP src/common.h

clean:
	rm -f bin/*
	rm -f bld/*
	rm -rf noter
